# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 
    Midterm-homework(firebase)
    Chatroom(Gitlab)
* Key functions (add/delete)
    1. Log in/ Sign up
    2. Chat Room
    3. Log Out
    
* Other functions (add/delete)
    1. Sign in with Google
    2. Chrome Notification
    3. New Message Notification
    4. Lobby
    5. Search Friend
    6. CSS Animation
    7. RWD

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-homework.web.app/

# Components Description : 
## 1. Log in/Sign up : 
![](https://i.imgur.com/cwhsQ0D.jpg)

(1) 輸入信箱及密碼後，按'Log In'按鈕可登入，登入後跳轉至chatroom
(3) 輸入信箱及密碼後，按'New Account'按鈕可直接註冊帳號，註冊完成後需要再Log In一次
(4) 按'Sign In With Google'按鈕可Google登入

## 2. Chat Room : 
![](https://i.imgur.com/ogdxgiQ.jpg)
(1) 登入後初始畫面為大廳
(2) 所有登入過的使用者都會自動加入好友
(3) 點擊右方朋友列表，可以選擇朋友並進入與他的聊天室
(4) 點擊功能表的'Lobby'按鈕或好友列表上方的'Lobby'均可回到大廳
(5) 聊天畫面最上方會顯示當前聊天室下朋友的信箱

## 3. Log Out : 
![](https://i.imgur.com/yTlG67p.png)
(1) 點擊功能表最右邊的'Account'會出現當前使用者帳號以及'Log Out'按鈕
(2)點擊'Log Out'按鈕即可登出，登出後會回到登入頁面
(3)若登出後回上一頁，會回到chatroom的頁面，但因為不是登入狀態，好友列表和聊天室訊息都會是空白的
(4)在未登入狀態時點擊'Account'按鈕只會出現'Log In'一個選項，點'Log In'按鈕會回到登入頁面



# Other Functions Description : 
## 1. Sign In With Google
*    可參見Log In/ Sign Up介紹
## 2. Chrome Notification
![](https://i.imgur.com/Us1lTYR.png)
*    當有人從非當前聊天室的私人聊天室傳送訊息時，使用者會收到對方傳送訊息給自己的提醒
*    如果重新整理之後還是沒有讀對方傳來的訊息，會再提醒一次
*    來自大廳的訊息不會提醒
## 3. New Message Notification
![](https://i.imgur.com/ODJnbVQ.png)

*    有非當前聊天室的未讀新訊息時，朋友列表上該朋友的欄位會出現一個紅點提醒，點擊該聊天室後紅點會消失
*    來自大廳的訊息不會提醒
## 4. Lobby
*    大廳，一個讓所有使用者自由聊天的地方
*    由於來自大廳的訊息可能會非常多，因此所有大廳的訊息都不會提醒使用者
## 5. Search Friend
![](https://i.imgur.com/UPe126g.png)
*    點擊功能表的'Friends'按鈕，下方會一個搜尋欄，再點一次可收回搜尋欄
*    在搜尋欄內輸入朋友的電子信箱，若這位朋友存在，則移至與該朋友的聊天室
*    若輸入自己的的電子信箱或是不存在的信箱，則會跳出對應的alert

## 6. CSS Animation
![](https://i.imgur.com/1HyG7fk.gif)
*    在Log In/ Sign Up頁面時，畫面左半部有一個旋轉的大風車
*    當螢幕寬度小於768px時，風車會出現在畫面下半部
## 7.RWD
### Log In/ Sign Up
*    大螢幕 : 左->風車 右->登入資訊
*    小螢幕 : 上->登入資訊 下->風車
### Chat Room
*    大螢幕 : 左->好友 右->聊天訊息
*    小螢幕 : 上->好友 下->聊天訊息

## Security Report (Optional)
### Deal With HTML code
![](https://i.imgur.com/HPbZ4l2.jpg)
*    將輸入訊息的特殊符號(ex.html tag <>)轉換成他所代表的編碼的一般字符
*    這樣一來就算是html tag也會被讀成一般的字符，就不會改變網頁的結構了

