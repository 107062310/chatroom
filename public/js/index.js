function initAPP(){
    var txtEmail = document.getElementById('inputemail');
    var txtPassword = document.getElementById('passwd');
    var btnLogin = document.getElementById('loginbtn');
    var btnGoogle = document.getElementById('googlebtn');
    var btnSignUp = document.getElementById('newbtn');
    var register = document.getElementById('login');

    var provider = new firebase.auth.GoogleAuthProvider();

    //Login
    btnLogin.addEventListener('click',function(){

        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(success){
            window.location.href = "chatroom.html";
        }).catch(function(error){
            var errorMsg = error.message;
            alert(errorMsg);
            txtPassword.value = '';
        });            

    });

    //Signup
    btnSignUp.addEventListener('click',function(){
        
            firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value)
            .then(function(success){
                alert("Create success :D");

                txtPassword.value = '';
            })
            .catch(function(error) {
                // Handle Errors here. 
                var errorMsg = error.message;
                alert(errorMsg);
                txtPassword.value = '';
            });             
        
  
    });

    //LoginWithGoogle
    btnGoogle.addEventListener('click', function() {
        
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API. 
            var token = result.credential.accessToken; 
            // The signed-in user info. 
            var user = result.user; 
            window.location.href = "chatroom.html";
        }).catch(function(error) { 
            // Handle Errors here. 
            var errorMsg = error.message;
            alert(errorMsg);
            txtPassword.value = '';
         });
    });
}

window.onload = function() {
    initAPP();
};