var currentroom = 'Lobby';
var postsRef='';
var str_before_username = '';
var str_before_username2 = '';
var str_after_content = '';
var user_name = '';
var user_uid = '';
var friend_uid = '';
var friend_name = '';
// List for store posts html
var total_post = [];
// Counter for checking history post update complete
var first_count = 0;
// Counter for checking when to update new post
var second_count = 0;

function init(){

    user_name = '';
    user_uid = '';

    //Chrome授權
    if (Notification && Notification.permission === 'default') {
        Notification.requestPermission(function (permission) {
            if (!('permission' in Notification)) {
                Notification.permission = permission;
            }
        });
    }
    else if (Notification.permission === 'granted') {
    }
    else {
        alert('請檢查瀏覽器支援');
    }

    firebase.auth().onAuthStateChanged(function(user){
    var menu = document.getElementById('menu');
        if(user){
            //Log Out
            user_name = user.email;
            user_uid = user.uid;
            menu.innerHTML = '<a class="dropdown-item" id="user_email" href="chatroom.html">'+user.email+'</a><a class="dropdown-item" href="#" id="logoutbtn">Log Out</a>'
            //user_email.innerHTML = user.email;
            var LogOut = document.getElementById("logoutbtn");
            LogOut.addEventListener('click',function(){
                console.log("logout!");
                firebase.auth().signOut().then(function(){
                    alert("Logout Success!");
                    window.location.href = "index.html";
                }).catch(function(){
                    alert("Logout Failed Q_Q");
                });
            });
            //update user list
            var UserRef = firebase.database().ref('user_list');
            var exist = false;
            UserRef.once('value').then(function(snapshot){
                for(let i in snapshot.val()){
                    if(snapshot.val()[i].email == user_name){
                        exist = true;
                    }
                }
                if(exist==false){
                    firebase.database().ref('user_list/'+ user_uid).set({
                        "myUID": user_uid,
                        "email": user_name
                    });
                }                
            }).catch(e => console.log(e.message));

            var str_before_friend = "<div class='myfriend'><div class='media text-muted pt-3'><img src='./css/src/test3.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    
        //friend
        var FriendRef = firebase.database().ref('user_list');
        var totalfriend = [];
        var friendBefore = 0;
        var friendAfter = 0;

        FriendRef.once('value')
        .then(function(snapshot){
            document.getElementById('myFriend').innerHTML = totalfriend.join('');
            var user = firebase.auth().currentUser;
            FriendRef.on('child_added',function(data){
                friendAfter += 1;
                if(friendAfter > friendBefore){
                    var ChildData = data.val();
                    if(ChildData.email != user.email){
                        var ChildUID = ChildData.myUID;
                        var str_after_friend = "</p><div style='width:5px;height:5px;background-color:transparent;border-radius:50%;' id='"+ChildUID + "'></div></div></div>\n";
                        totalfriend[totalfriend.length] = '<button onclick="changeRoom(`'+ChildUID+'`)"style = "width:100%; border:none; background-image: linear-gradient(120deg, #e0c3fc 0%, #8ec5fc 100%);">' + str_before_friend + ChildData.email + str_after_friend+'</button>';
                        var ch = document.getElementById('myFriend');
                        ch.innerHTML = totalfriend.join('');
                    }
                }
            });
            

                var myRoom = firebase.database().ref('user_list/'+user_uid+'/notice');
                console.log(user_uid+'~');

                myRoom.once('value').then(function(snapshot){
                    myRoom.on("child_added",function(snap){
                        console.log('new msg!');
                        var newmsg = snap.val();
                        var friend = newmsg.from;
                        var fromID = newmsg.from_id;
                        document.getElementById(fromID).style.background='red';
                        console.log(friend);
                        var msg = friend + ' sends you a message!';
                        var notifyConfig = {
                            body: msg // 設定內容
                        };
                        if (Notification.permission === 'granted') {
                            var notification = new Notification('New Message!', notifyConfig); // 建立通知
                            notification. onclick = function(){
                                changeRoom(fromID);
                            }
                        }
                    });                
                }).catch(e => console.log(e.message));
        }).catch(e => console.log(e.message));



        }else{
            menu.innerHTML = '<a class="dropdown-item" href="index.html">Log In</a>'
            document.getElementById('myFriend').innerHTML = "";
            document.getElementById('chatting').innerHTML = "";
        }
    });

    post_btn = document.getElementById('submit');
    post_txt = document.getElementById('txt');

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            var time = new Date();
            var year = time.getFullYear();
            var month = time.getMonth()+1;
            var day = time.getDate();
            var hr = time.getHours();
            var min = time.getMinutes();
            var now = year+'/'+month+'/'+day+' '+hr+':'+min;
            var user = firebase.auth().currentUser;
            if(currentroom == 'Lobby'){
                firebase.database().ref('com_list/').push({
                    email: user.email, 
                    data: post_txt.value,
                    time: now
                });                
            }else{
                firebase.database().ref('room/'+currentroom+'/message/').push({
                    email: user.email, 
                    data: post_txt.value,
                    time: now
                });
                if(currentroom!="Lobby"){
                    firebase.database().ref('user_list/'+ friend_uid +'/room/' +currentroom+'/').update({
                        newMsg: 'True'
                    });   
                firebase.database().ref('user_list/'+friend_uid + '/notice/'+currentroom).update({
                    notice:'Yes',
                    from: user_name,
                    from_id :user_uid
                });                                     
                }
            }
            post_txt.value = "";
        }
    });

    // The html code for post
    str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3' style='width:65%;'><img src='./css/src/test.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    str_before_username2 = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3' style =' background-color:#E6CAFF; width:65%; float:right; border-radius:5px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray' style = 'text-align: right;'><strong class='d-block text-gray-dark'>";
    str_after_content = "</p></div></div>\n";

    if(currentroom == 'Lobby'){console.log('!');postsRef = firebase.database().ref('com_list');}
    else{postsRef = firebase.database().ref('room/'+roomkey+'/message');}


    load();

    //friend list
    
}

function changeRoom(person){

    var user = firebase.auth().currentUser;
    var user_uid = user.uid;
    var Ref = firebase.database().ref('user_list/'+ user_uid+'/room');
    var exist = false;
    var roomkey;
        Ref.once('value').then(function(snapshot){
            for(let i in snapshot.val()){
                if(snapshot.val()[i].friend == person){
                    exist = true;
                    roomkey = snapshot.val()[i].roomkey;
                    var email = htmlspecialchars(snapshot.val()[i].friend_email);
                    var wd = '<p style="color: white;">'+email+'</p>';
                    console.log(wd);
                    document.getElementById('roomname').innerHTML = wd;
                     
                    break;
                }
            }
            if(exist==false){

                var friend_info = firebase.database().ref('user_list/'+person);
                friend_info.once('value',function(data){
                    friend_name = data.val().email;
                    console.log(friend_name);
                })
                var email = htmlspecialchars(friend_name);
                var wd2 = '<p style="color: white;">'+email+'</p>';
                document.getElementById('roomname').innerHTML = wd2;
                roomkey = firebase.database().ref('user_list/'+ user_uid + '/room').push({
                    "friend":person,
                    "friend_email":friend_name,
                    "newMsg":'False'
                }).key;
                firebase.database().ref('user_list/'+ user_uid + '/room/'+ roomkey).update({
                    "roomkey": roomkey
                });
                firebase.database().ref('user_list/'+ person + '/room/'+ roomkey).update({
                    "friend": user_uid,
                    "friend_email":user_name,
                    "roomkey": roomkey,
                    "newMsg":  'False'
                });
                firebase.database().ref('room/'+roomkey).update({
                    "user1": user_name,
                    "user2": friend_name
                });
                nowfriend = firebase.database().ref('room/'+roomkey);
            }
            
            friend_uid = person;
            console.log(roomkey);
            currentroom = roomkey;
            console.log("currentroom:"+currentroom);
            postsRef.off();
            postsRef = firebase.database().ref('room/'+roomkey+'/message');
            document.getElementById('chatting').innerHTML = ' ';
            total_post = [];
            first_count = 0;
            second_count = 0;
            load();
        }).catch(e => console.log(e.message));
}

function ChangeLobby(){
    console.log("!!!");
    currentroom = 'Lobby';
    document.getElementById('roomname').innerHTML = '<p style="color: white;">Lobby</p>';
    postsRef.off();
    postsRef = firebase.database().ref('com_list');
    document.getElementById('chatting').innerHTML = ' ';
    total_post = [];
    first_count = 0;
    second_count = 0;
    load();
}

//load message
function load(){
    str_before_username = "<div class='rounded box-shadow' style='width:100%'><div class='media text-muted' style='margin-left:5px; margin-right:20px;' ><p class='msg'><span class='arrow_l_in'></span><span class='arrow_l_out'></span><strong class='d-block text-gray-dark'>";
    str_before_username2 = "<div class='row rounded box-shadow justify-content-end' style='width:100%'><div class='column media text-muted' style='margin-left:20px; margin-right:5px;'><p class='msg my'><span class='arrow_r_in'></span><span class='arrow_r_out'></span><strong class='d-block text-gray-dark'>";
    str_after_content = "</p></div></div>\n";
    postsRef.once('value')
        .then(function(snapshot) {
            /// Join all post in list to html in once
            document.getElementById('chatting').innerHTML = total_post.join('');
            var user = firebase.auth().currentUser;


            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                if(currentroom!='Lobby'){
                    firebase.database().ref('user_list/'+ user_uid +'/room/' +currentroom+'/').update({
                        newMsg: 'False'
                    });
                    var del = firebase.database().ref('user_list/'+ user_uid +'/notice/' +currentroom);
                    document.getElementById(friend_uid).style.background='transparent';
                    del.remove();

                }
                second_count += 1;
                if (second_count > first_count) {
                    if(currentroom!='Lobby'){
                        firebase.database().ref('user_list/'+ user_uid +'/room/' +currentroom+'/').update({
                            newMsg: 'False'
                        });    
                    }
                    var childData = data.val();
                    var childtxt = htmlspecialchars(childData.data);
                    if(childData.email == user.email){
                        total_post[total_post.length] = str_before_username2+ childData.email + "</strong>" + childtxt + str_after_content;

                    }else{
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childtxt + str_after_content;
                    }

                    var ch = document.getElementById('chatting'); 
                    ch.innerHTML = total_post.join('');
                    ch.scrollTop = ch.scrollHeight;
                }
            });
        })
        .catch(e => console.log(e.message));
}

function htmlspecialchars(str)  
{  
    str = str.replace(/&/g, '&amp;');
    str = str.replace(/</g, '&lt;');
    str = str.replace(/>/g, '&gt;');
    str = str.replace(/"/g, '&quot;');
    str = str.replace(/'/g, '&#039;');
    return str;
}

function SearchFriend(){
    var searchData = document.getElementById('inputbox');
    if(searchData.value!=''){
        var myfriend = searchData.value;
        var chatList = firebase.database().ref('user_list');
        var exist=false;
        chatList.once('value').then(function(snapshot){
            for(let i in snapshot.val()){
                if(snapshot.val()[i].email == searchData.value){
                    exist = true;
                    if(myfriend == user_name){
                        alert("去找朋友!!!");
                    }else{
                        alert("Let's chat with " + myfriend +"!!!!");

                        changeRoom(snapshot.val()[i].myUID);
                    }
                    searchData.value = '';
                    break;
                }
            } 
            if(!exist){
                alert("你根本沒有這個朋友!!!");
                searchData.value = '';
            }              
            }).catch(e => console.log(e.message));
        }

}

window.onload = function() {
    init();
};
